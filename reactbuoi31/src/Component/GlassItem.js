import React, { Component } from 'react'

export default class  extends Component {
  render() {
    return (
      <img src={this.props.glasses.url} alt="Glasses" className='img-fluid' 
        onClick={()=>this.props.changeGlasses(this.props.glasses)}
      />
    )
  }
}
