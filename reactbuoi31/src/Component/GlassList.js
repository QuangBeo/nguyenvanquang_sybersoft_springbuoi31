import React, { Component } from 'react'
import GlassItem from './GlassItem';

export default class GlassList extends Component {
    render() {
      let {glasses} = this.props;
      let RenderGlass = () => {
        return glasses.map((item,index) => {
            return <div className="col-2" key={index}>
                <GlassItem glasses={item} changeGlasses={this.props.changeGlasses}/>
            </div>
        })
      }
    return (
      <div className='row py-5'>
        {RenderGlass()}
      </div>
    )
  }
}
