import React, { Component } from 'react'
import { dataGlass } from './dataGlasses'
import GlassList from './GlassList'

export default class Content extends Component {
    state = {
        glasses: {
            "id": 1,
            "price": 30,
            "name": "GUCCI G8850U",
            "url": "./glassesImage/v1.png",
            "desc": "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. "
        }
    }
    changeGlasses = (newGlasses) => {
        this.setState({
            glasses: newGlasses,
        })
    }
  render() {
    return (
      <div className='content'>
        <div className="header bg-dark p-4" style={{
            opacity:"70%",
        }}>
            <h3 className='text-center text-white'>TRY CLASSES APP ONLINE</h3>
        </div>
        <div className="container position-relative">
            <div className="content d-flex justify-content-around">
                <div className="show-content position-relative">
                    <img src="./glassesImage/model.jpg" alt="" style={{
                        width:"350px",
                        height:"400px",
                    }}/>
                    <div className="content-info" style={{
                        bottom:"0",
                        left:"0",
                        width:"100%",
                        position: "absolute",
                        color:"white",
                        background:"rgba(0,0,0,0.4)" 
                    }}>
                        <h4 className=''>{this.state.glasses.name}</h4>
                        <p>{this.state.glasses.desc}</p>
                    </div>
                    <img src={this.state.glasses.url} alt="" style={{
                        position:"absolute",
                        top:"106px",
                        width:"170px",
                        left:"90px"
                    }}/>
                </div>
                <img src="./glassesImage/model.jpg" alt="" style={{width:"350px",height:"400px"}}/>
            </div>
        </div>
        <div className="footer container">
            <GlassList glasses={dataGlass} changeGlasses={this.changeGlasses}/>
        </div>
      </div>
    )
  }
}
